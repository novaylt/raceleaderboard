@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">

                <div class="panel-body">
                    
            <div class="box-header">
              <h3 class="box-title">SPRINT LEADERBOARD </h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                </div>
              </div>
            </div>

                     @if(Session::has('runner_delete'))
                             <div class="alert alert-danger">
                                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-trash" aria-hidden="true"></i> {{ session('runner_delete') }}
                             </div>
                         @endif

                     @if(Session::has('runner_time_delete'))
                             <div class="alert alert-danger">
                                 <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-trash" aria-hidden="true"></i> {{ session('runner_time_delete') }}
                             </div>
                         @endif


                        @if(Session::has('runner_updated'))
                            <div class="alert alert-success">
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-check-circle-o" aria-hidden="true"></i> {{ Session::get('runner_updated') }}
                          </div>                  
                         @endif 

                        @if(Session::has('time_saved'))
                            <div class="alert alert-success">
                               <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><i class="fa fa-check-circle-o" aria-hidden="true"></i> {{ Session::get('time_saved') }}
                          </div>                  
                         @endif 

  @if (count($TopFastestTime)) 

            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                            <thread>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Email</th>
                              <th>DOB</th>
                              <th>Fastest Time <small>(ss:mm)</small></th>
                              <th>Manage</th>
                            </tr>
                           </thread>


                             @foreach($TopFastestTime as $key => $AllFastTimes)
                             <thread>
                                   <tr>
                                     <td>{{ $AllFastTimes->firstname }}</th>
                                     <td>{{ $AllFastTimes->lastname }}</th>
                                     <td>{{ $AllFastTimes->email }}</th>
                                     <td>{{ $AllFastTimes->dob }}</th>
                                     <td>{{ $AllFastTimes->fastesttime }}</th>
                                     <td>{{ Form::open(['route' => ['home', $AllFastTimes->id], 'method' => 'delete']) }}
                                          <input type="hidden" name="_method" value="DELETE">
                                          <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                                        {{ Form::close() }} 
                                     </th>
                                   </tr>
                                </thread>
                             @endforeach
                         

  @else  
                                 <div align="center">
                                   <br></br>
                                   <strong><h3><b>NO SPRINT TIMES RECORDED!</b></h3></strong></p>
                                 </div>
  @endif   

             </table>
            </div>
   </div>
</div>


        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                 <div class="panel-body">
                                <div class="box-header">
                                  <h3 class="box-title">INPUT TIME <small>(Use Controls or Enter Manually)</small> </h3>

                                  <div class="box-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                    </div>
                                  </div>
                                </div>


                                 <div class="box-body table-responsive no-padding">
                                   <table class="table table-hover">
                                        <thread>
                                          <th>Runner</th>
                                          <th>Time <small>(ss:mm)</small></th>
                                          <th>Controls</th>
                                          <th>Manage</th>
                                        </tr>
                                       </thread>

                                   <form class="form-horizontal" name="timer" role="form" method="POST" action="{{route('home.time') }}">
                                       {!! csrf_field() !!}
                                             <thread>
                                                   <tr>
                                                     <td>{!! Form::select('runner', $ListRunners, null) !!}</th>
                                                     <td>{!! Form::text('time', null, ['class' => 'form-control']) !!}</th>
                                                     <td> <span>
                                                            <input type="button" class="btn btn-primary" value="Start" onclick="starttimer()" >
                                                            <input type="button" class="btn btn-danger" value="Stop" onclick="stoptimer()">
                                                            <input type="reset" class="btn btn-info" onclick="resettimer()">
                                                          </span></th>   
                                                     <td>{{ Form::submit('SAVE', array('class' => 'btn btn-info')) }}</th>
                                                   </tr>
                                                </thread>                                                                        
                                    </form>
                                 </table>
                                </div>
                   </div>
              </div>       
        </div>


    </div>
</div>

<script>

var millisec = 0;
var seconds = 0;
var idleseconds = 0;
var timer;

function display(){

  if (millisec>=9){
     millisec=0
     seconds+=1
  }
  if
    (seconds>=10){
     seconds=0
     idleseconds+=1
  }
  else
     millisec+=1
     document.timer.time.value =  idleseconds + "" + seconds + ":" + millisec;
     timer = setTimeout("display()",100);
  }


function stoptimer() {
  clearTimeout(timer);
  timer = 0;
   var startVar = setInterval(myTimer);
   var d = new Date();
}

function starttimer() {
  if (timer > 0) {
  } else {
     display();
     
     var startVar = setInterval(myTimer);
     var d = new Date();
  }
}

function resettimer() {
  stoptimer();
  millisec = 0;
  seconds = 0;
  idleseconds = 0;
}


var myVar = setInterval(myTimer ,1000);
function myTimer() {
    var d = new Date();
}

</script>



@endsection

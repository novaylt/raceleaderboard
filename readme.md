# Race Leader Board
Race Leader Board is a simple web application developed using the Laravel MVC Framework. It's features are;
to allow a user register and login,
create runner contacts,
delete runner contacts,
record a runners time manually or by using a stoptimer,
save a runners lap time,
delete a runners laptime,
display the lap times of all runners of the current day on the home page.

## Contact Me

If you have any questions regarding this project, please email Ibrahim Ahmed at ibrahim_345@outlook.com


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
